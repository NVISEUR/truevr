﻿namespace HellionCat.TrueVR.Debug
{
    public static class TVRLogger
    {
        private static string s_prefix = "TrueVR : ";
        private static bool s_enabled = false;

        /// <summary>
        /// Enable logging for this session
        /// </summary>
        public static void Enable()
        {
            s_enabled = true;
        }

        /// <summary>
        /// Log the given message in the console
        /// </summary>
        /// <param name="p_message">The message you wish to send to the console</param>
        public static void LogMessage(string p_message)
        {
            if (!s_enabled) return;
        
            UnityEngine.Debug.Log(s_prefix+p_message);
        }

        /// <summary>
        /// Log the given message in the console, using the given color if possible
        /// </summary>
        /// <param name="p_message">The message you wish to send to the console</param>
        /// <param name="p_color">The color given to the text if possible</param>
        public static void LogMessage(string p_message, string p_color)
        {
            if (!s_enabled) return;
        
            UnityEngine.Debug.Log("<color="+p_color+">"+s_prefix+p_message+"</color>");
        }
    
        /// <summary>
        /// Log the given warning in the console
        /// </summary>
        /// <param name="p_message">The message you wish to send to the console</param>
        public static void LogWarning(string p_message)
        {
            if (!s_enabled) return;
        
            UnityEngine.Debug.LogWarning(s_prefix+p_message);
        }

        /// <summary>
        /// Log the given warning in the console, using the given color if possible
        /// </summary>
        /// <param name="p_message">The message you wish to send to the console</param>
        /// <param name="p_color">The color given to the text if possible</param>
        public static void LogWarning(string p_message, string p_color)
        {
            if (!s_enabled) return;
        
            UnityEngine.Debug.LogWarning("<color="+p_color+">"+s_prefix+p_message+"</color>");
        }
    
        /// <summary>
        /// Log the given error in the console
        /// </summary>
        /// <param name="p_message">The message you wish to send to the console</param>
        public static void LogError(string p_message)
        {
            if (!s_enabled) return;
        
            UnityEngine.Debug.LogError(s_prefix+p_message);
        }

        /// <summary>
        /// Log the given error in the console, using the given color if possible
        /// </summary>
        /// <param name="p_message">The message you wish to send to the console</param>
        /// <param name="p_color">The color given to the text if possible</param>
        public static void LogError(string p_message, string p_color)
        {
            if (!s_enabled) return;
        
            UnityEngine.Debug.LogError("<color="+p_color+">"+s_prefix+p_message+"</color>");
        }
    }
}
