﻿using UnityEngine;

namespace HellionCat.TrueVR.Debug
{
    public class TVRDebugManager : MonoBehaviour
    {
        [Header("Select which options are active in a development build")]
        [SerializeField] private bool m_visualizeCollider;
        [SerializeField] private bool m_logMessages;
        
#if DEVELOPMENT_BUILD
    
        void Start()
        {
            //Let some time for the other systems to load
            Invoke(nameof(InitializeDebugging),1f);
        }

        void InitializeDebugging()
        {
            
            if (m_visualizeCollider)
            {
                //Enable collider visualization
                foreach (var l_tvrColliderVisualizer in FindObjectsOfType<TVRColliderVisualizer>())
                {
                    l_tvrColliderVisualizer.Visualize();
                }
            }

            if (m_logMessages)
            {
                //Enable logging in the console
                TVRLogger.Enable();
            }
        }
#endif
    }
}
