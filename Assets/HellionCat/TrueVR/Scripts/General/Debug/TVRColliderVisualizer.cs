﻿
using UnityEngine;

namespace HellionCat.TrueVR.Debug
{
    /// <summary>
    /// This component is used to visualize a collider in build.
    /// Has for now, it supports both box collider and capsule collider, and may support other colliders if needed
    /// </summary>
    public class TVRColliderVisualizer : MonoBehaviour
    {
        [Header("Ensure the collider keep its state updated each frame")]
        [SerializeField] private bool m_keepUpdated = true;
        [Header("Use this to get another color and differentiate more easily two collider")] 
        [SerializeField] private bool m_otherMaterial;
        
        private Collider m_collider;
        private Material m_material;

        private GameObject m_child;
        
        #if DEVELOPMENT_BUILD
        private void Start()
        {
            if (m_otherMaterial)
            {
                m_material = Resources.Load("Materials/Debug/m_colliderGhost", typeof(Material)) as Material;
            }
            else
            {
                m_material = Resources.Load("Materials/Debug/m_collider", typeof(Material)) as Material;
            }

            m_collider = GetComponent<Collider>();
        }
        #endif
    
        /// <summary>
        /// Display the collider in game for debug purpose
        /// </summary>
        public void Visualize()
        {
            if (m_child != null)
            {
                if (m_collider is BoxCollider l_boxCollider)
                {
                    m_child.transform.localPosition = l_boxCollider.center;
                    m_child.transform.localScale = l_boxCollider.size;
                }
                else if (m_collider is CapsuleCollider l_capsuleCollider)
                {
                    m_child.transform.localPosition = l_capsuleCollider.center;
                    m_child.transform.localScale = new Vector3(l_capsuleCollider.radius*2,l_capsuleCollider.height/2,l_capsuleCollider.radius*2);
            
                    if (l_capsuleCollider.direction == 0)
                    {
                        m_child.transform.localRotation = Quaternion.Euler(0,0,90);
                    }
                    else if (l_capsuleCollider.direction == 2)
                    {
                        m_child.transform.localRotation = Quaternion.Euler(90,0,0);
                    }
                }
                else if (m_collider is SphereCollider l_sphereCollider)
                {
                    m_child.transform.localPosition = l_sphereCollider.center;
                    m_child.transform.localScale = Vector3.one*l_sphereCollider.radius;
                }
            }
            else
            {
                GameObject l_gameObject = null;
                if (m_collider is BoxCollider l_boxCollider)
                {
                    l_gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    //We do not need the collider
                    Destroy(l_gameObject.GetComponent<Collider>());
                    l_gameObject.transform.parent = transform;
                    l_gameObject.transform.localPosition = l_boxCollider.center;
                    l_gameObject.transform.localScale = l_boxCollider.size;
                }
                else if (m_collider is CapsuleCollider l_capsuleCollider)
                {
                    l_gameObject = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                    //We do not need the collider
                    Destroy(l_gameObject.GetComponent<Collider>());
                    l_gameObject.transform.parent = transform;
                    l_gameObject.transform.localPosition = l_capsuleCollider.center;
                    l_gameObject.transform.localScale = new Vector3(l_capsuleCollider.radius*2,l_capsuleCollider.height/2,l_capsuleCollider.radius*2);
            
                    if (l_capsuleCollider.direction == 0)
                    {
                        l_gameObject.transform.localRotation = Quaternion.Euler(0,0,90);
                    }
                    else if (l_capsuleCollider.direction == 2)
                    {
                        l_gameObject.transform.localRotation = Quaternion.Euler(90,0,0);
                    }
                }
                else if (m_collider is SphereCollider l_sphereCollider)
                {
                    l_gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    //We do not need the collider
                    Destroy(l_gameObject.GetComponent<Collider>());
                    l_gameObject.transform.parent = transform;
                    l_gameObject.transform.localPosition = l_sphereCollider.center;
                    l_gameObject.transform.localScale = Vector3.one*l_sphereCollider.radius;
                }

                if (l_gameObject != null)
                {
                    MeshFilter l_meshFilter = l_gameObject.GetComponent<MeshFilter>(); 
                    MeshRenderer l_meshRenderer = l_gameObject.GetComponent<MeshRenderer>();

                    //Make a copy of the mesh to allow modification without overrinding other gameobject with the same mesh
                    var l_mesh = l_meshFilter.mesh;
                    Mesh l_clone = new Mesh
                    {
                        name = "clone",
                        vertices = l_mesh.vertices,
                        triangles = l_mesh.triangles,
                        normals = l_mesh.normals,
                        uv = l_mesh.uv
                    };

                    //Change the way the mesh is visualized by drawing lines
                    for (int l_i = 0; l_i < l_clone.subMeshCount; l_i++)
                    {
                        l_clone.SetIndices(l_clone.GetIndices(l_i),MeshTopology.Lines,l_i);
                    }
                    l_meshFilter.mesh = l_clone;
                    l_meshRenderer.material = m_material;
                }

                m_child = l_gameObject;
            }
        }

#if DEVELOPMENT_BUILD
        private void Update()
        {
            if (!m_keepUpdated || !m_collider || !m_child) return;
            
            if (m_collider.enabled == false && m_child.activeInHierarchy)
            {
                m_child.SetActive(false);
            }
           
            if (m_collider.enabled && !m_child.activeInHierarchy)
            {
                m_child.SetActive(true);
            }

            Visualize();
        }
#endif
    }
}
