﻿using UnityEngine;
using UnityEngine.XR;

namespace HellionCat.TrueVR.General
{
    /// <summary>
    /// Adapt the fixed delta time to the current xr device refresh rate to ensure a smooth experience.
    /// </summary>
    public class TVRDeltaTimeAdapter : MonoBehaviour
    {
        void Update()
        {
            if (XRDevice.isPresent)
            {
                Time.fixedDeltaTime = (Time.timeScale / XRDevice.refreshRate);
            }
            else
            {
                Time.fixedDeltaTime = Time.timeScale / 60.0f;
            }
        }
    }
}
