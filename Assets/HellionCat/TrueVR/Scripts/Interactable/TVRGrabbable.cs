﻿using HellionCat.TrueVR.Avatar;
using OculusSampleFramework;
using UnityEngine;

namespace HellionCat.TrueVR.Interactable
{
    public class TVRGrabbable : MonoBehaviour
    {
        [SerializeField] private TVRCustomPose m_customPose;
        
        private const string LeftHandGrabParentTag = "LeftHandGrabParent";
        private const string RightHandGrabParentTag = "RightHandGrabParent";

        private static Transform s_leftHandGrabParent;
        private static Transform s_rightHandGrabParent;

        private bool m_grabbedByLeftHand;
        private bool m_grabbedByRightHand;

        private Transform m_transform;
        private Rigidbody m_rigidbody;
        private Rigidbody m_leftHandRigidBody;
        private Rigidbody m_rightHandRigidBody;

        private void Start()
        {
            s_leftHandGrabParent = GameObject.FindWithTag(LeftHandGrabParentTag).transform;
            s_rightHandGrabParent = GameObject.FindWithTag(RightHandGrabParentTag).transform;

            var l_hands = FindObjectsOfType<Hand>();
            
            for (var l_i = 0; l_i < l_hands.Length; l_i++)
            {
                if (l_hands[l_i].HandType == OVRPlugin.Hand.HandLeft)
                {
                    m_leftHandRigidBody = l_hands[l_i].GetComponent<Rigidbody>();
                }
                else
                {
                    m_rightHandRigidBody = l_hands[l_i].GetComponent<Rigidbody>();
                }
            }

            m_transform = transform;
            m_rigidbody = GetComponent<Rigidbody>();
        }

        public void Grab(OVRPlugin.Hand p_hand)
        {
            if (p_hand == OVRPlugin.Hand.HandLeft)
            {
                if (m_grabbedByLeftHand) return;
                
                if(m_grabbedByRightHand)
                    Drop(OVRPlugin.Hand.HandRight,true);

                m_grabbedByLeftHand = true;
                
                m_transform.parent = s_leftHandGrabParent;
            }
            else if (p_hand == OVRPlugin.Hand.HandRight)
            {
                if (m_grabbedByRightHand) return;
                
                if(m_grabbedByLeftHand)
                    Drop(OVRPlugin.Hand.HandLeft,true);

                m_grabbedByRightHand = true;
                
                m_transform.parent = s_rightHandGrabParent;
            }

            m_rigidbody.isKinematic = true;

            if (m_customPose)
                SetHandPose();
        }

        private void SetHandPose()
        {
            m_transform.localPosition = m_customPose.CustomLocalPosition;
            HandSkeleton l_handSkeleton = null;
            l_handSkeleton = m_grabbedByLeftHand ? m_leftHandRigidBody.GetComponentInChildren<HandSkeleton>() : m_rightHandRigidBody.GetComponentInChildren<HandSkeleton>();

            if (!l_handSkeleton)
                return;
            
            for (var l_i = 0; l_i < m_customPose.Data.Length; l_i++)
            {
                l_handSkeleton.ChangeBoneFreezingState((int)m_customPose.Data[l_i].BoneId,m_customPose.Data[l_i].Frozen,true);
                l_handSkeleton.RenderBones[(int) m_customPose.Data[l_i].BoneId].localRotation = Quaternion.Euler(m_customPose.Data[l_i].Rotation);
            }
        }

        private void ResetHandPose()
        {
            if (m_grabbedByLeftHand)
            {
                m_leftHandRigidBody.GetComponentInChildren<HandSkeleton>().UnFreezeAll();
            }
            else
            {
                m_rightHandRigidBody.GetComponentInChildren<HandSkeleton>().UnFreezeAll();
            }
        }

        public void Drop(OVRPlugin.Hand p_hand, bool p_changingHand = false)
        {
            if (p_hand == OVRPlugin.Hand.HandLeft)
            {
                if (!m_grabbedByLeftHand) return;

                ResetHandPose();
                m_grabbedByLeftHand = false;

                if (!p_changingHand)
                {
                    //Dropped
                    m_transform.parent = null;
                    m_rigidbody.isKinematic = false;
                    m_rigidbody.velocity = m_leftHandRigidBody.velocity;
                    m_rigidbody.angularVelocity = m_leftHandRigidBody.angularVelocity;
                }
            }
            else if (p_hand == OVRPlugin.Hand.HandRight)
            {
                if (!m_grabbedByRightHand) return;

                ResetHandPose();
                m_grabbedByRightHand = false;

                if (!p_changingHand)
                {
                    //Dropped
                    m_transform.parent = null;
                    m_rigidbody.isKinematic = false;
                    m_rigidbody.velocity = m_rightHandRigidBody.velocity;
                    m_rigidbody.angularVelocity = m_rightHandRigidBody.angularVelocity;
                }
            }
        }
    }
}
