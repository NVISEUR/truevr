﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HellionCat.TrueVR.Interactable;
using UnityEngine;

public class TVRGrabbableManager : MonoBehaviour
{
    private List<Collider> m_grabbables;
    private static TVRGrabbableManager s_instance;

    public static TVRGrabbableManager Instance => s_instance;

    private void Awake()
    {
        if (s_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            s_instance = this;
        }
        
        m_grabbables = new List<Collider>();

        foreach (var l_tvrGrabbable in FindObjectsOfType<TVRGrabbable>())
        {
            m_grabbables.Add(l_tvrGrabbable.GetComponentInChildren<Collider>());
        }
    }

    public TVRGrabbable GetObjectIntersecting(Bounds p_bounds)
    {
        List<TVRGrabbable> l_grabbables = new List<TVRGrabbable>();

        for (var l_i = 0; l_i < m_grabbables.Count; l_i++)
        {
            if (m_grabbables[l_i].bounds.Intersects(p_bounds))
            {
                l_grabbables.Add(m_grabbables[l_i].GetComponentInParent<TVRGrabbable>());
            }
        }

        //We grab the object which center is the closest to our fingers
        var l_orderedEnumerable = l_grabbables.OrderBy(g => Vector3.Distance(g.transform.position, p_bounds.center)).ToList();

        //If only one collisions
        if (l_orderedEnumerable.Count > 0)
        {
            return l_orderedEnumerable[0];
        }

        return null;
    }
    
}
