﻿using System;
using HellionCat.TrueVR.Interactable;
using OculusSampleFramework;
using UnityEngine;

namespace HellionCat.TrueVR.Avatar
{
    public class TVRGrabber : MonoBehaviour
    {
        [SerializeField] private TVRHandGestureEvents m_handGestureEvents;
        [SerializeField] Hand m_hand;
        private TVRGrabbable m_grabbedObject;
        private Collider m_collider;

        public TVRGrabbable GrabbedObject => m_grabbedObject;

        private void Start()
        {
            m_handGestureEvents.OnStartPinchedIndex += TryGrabObject;
            m_handGestureEvents.OnStartPinchedMiddle += TryGrabObject;
            m_handGestureEvents.OnStartPinchedPinky += TryGrabObject;
            m_handGestureEvents.OnStartPinchedRing += TryGrabObject;

            m_handGestureEvents.StopGrabbing += TryDropObject;

            m_collider = GetComponent<Collider>();
        }

        private void TryGrabObject()
        {
            m_grabbedObject =
                TVRGrabbableManager.Instance.GetObjectIntersecting(m_collider.bounds);
            if (m_grabbedObject != null)
            {
                //If we are, then we are telling it that we are grabbing him
                m_grabbedObject.Grab(m_hand.HandType);
            }
        }

        private void TryDropObject()
        {
            if (m_grabbedObject != null)
            {
                m_grabbedObject.Drop(m_hand.HandType);
                m_grabbedObject = null;
            }
        }
    }
}
