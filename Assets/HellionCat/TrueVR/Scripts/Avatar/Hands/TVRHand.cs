﻿using System;
using System.Collections.Generic;
using System.Linq;
using HellionCat.TrueVR.Debug;
using HellionCat.TrueVR.Interactable;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HellionCat.TrueVR.Avatar
{
    public class TVRHand : MonoBehaviour
    {
        public const float TriggerDebounceTime = 0.05f;
        public const float ThumbDebounceTime = 0.15f;
        
        [Header("Settings")]
        [SerializeField] private Transform m_ovrCameraRigTransform;
        [SerializeField] private OVRInput.Controller m_controller;
        [SerializeField] private TVRHandGestureEvents m_handGestureEvents;
        //[SerializeField] private Transform m_ghostHandTransform;

        public OVRInput.Controller Controller => m_controller;

        private Rigidbody m_rigidBody;
        private Collider[] m_colliders;
        private Collider[] m_ghostColliders;
        private List<Renderer> m_showAfterInputFocusAcquired;
        private bool m_collisionEnabled;
        private bool m_restoreOnInputAcquired;
        private float m_collisionScaleCurrent;
        private Quaternion m_anchorOffsetRotation;
        private Vector3 m_anchorOffsetPosition;
        private TVRGrabbable m_grabbedObject;
    
        protected virtual void Awake()
        {
            //Setup the basic properties of the hand
            //m_rigidBody = GetComponent<Rigidbody>();
            //Transform l_transform = transform;
            //m_anchorOffsetPosition = l_transform.localPosition;
            //m_anchorOffsetRotation = l_transform.localRotation;
            //m_showAfterInputFocusAcquired = new List<Renderer>();
            //m_colliders = GetComponentsInChildren<Collider>().Where(p_childCollider => !p_childCollider.isTrigger).ToArray();
            //m_ghostColliders = GetComponentsInChildren<Collider>().Where(p_childCollider => p_childCollider.isTrigger).ToArray();
                //Directly enable collision on the whole hand
            //CollisionEnable(true);
            
            //Subscribe to the focus acquired/lost to show/hide the hands accordingly
            //OVRManager.InputFocusAcquired += OnInputFocusAcquired;
            //OVRManager.InputFocusLost += OnInputFocusLost;
            
            //Subscribe to the camera rig to get notified from controller position and rotation update
            if (m_ovrCameraRigTransform != null)
            {
                //OVRCameraRig l_cameraRig = m_ovrCameraRigTransform.GetComponent<OVRCameraRig>();
                //l_cameraRig.useFixedUpdateForTracking = true;
                //l_cameraRig.UpdatedAnchors += p_r => {OnUpdatedAnchors();};
            }
            else
            {
                TVRLogger.LogError("One of your hands miss a reference to the OvrCameraRig");
            }
            
            //Send an Oculus event
            #if UNITY_EDITOR
                        OVRPlugin.SendEvent("custom_hand", (SceneManager.GetActiveScene().name == "CustomHands").ToString(), "sample_framework");
            #endif
            

            m_handGestureEvents.StopGrabbing += TryDropObject;
        }

        private void TryGrabObject(Vector3 p_centerPoint)
        {
            UnityEngine.Debug.Log("Pinch");
            m_grabbedObject =
                TVRGrabbableManager.Instance.GetObjectIntersecting(new Bounds());
            if (m_grabbedObject != null)
            {
                UnityEngine.Debug.Log("Try grab");
                //If we are, then we are telling it that we are grabbing him
                //m_grabbedObject.Grab(m_controller);
            }
        }

        private void TryDropObject()
        {
            UnityEngine.Debug.Log("Unpinch");
            if (m_grabbedObject != null)
            {
                UnityEngine.Debug.Log("Try drop");
                //m_grabbedObject.Drop(m_controller);
                m_grabbedObject = null;
            }
        }

        /// <summary>
        /// Move the hands accordingly to the controller position and rotation
        /// </summary>
        /*private void OnUpdatedAnchors()
        {
            //Get the controller position and rotation
            Vector3 l_handPos = OVRInput.GetLocalControllerPosition(m_controller);
            Quaternion l_handRot = OVRInput.GetLocalControllerRotation(m_controller);
            //Define the position and rotation relative to the set "parent"
            Vector3 l_destPos = m_ovrCameraRigTransform.TransformPoint(m_anchorOffsetPosition + l_handPos);
            Quaternion l_destRot = m_ovrCameraRigTransform.rotation * l_handRot * m_anchorOffsetRotation;
            //Move and rotate the hand accordingly
            m_rigidBody.MovePosition(l_destPos);
            m_rigidBody.MoveRotation(l_destRot);
        }
    
        /// <summary>
        /// Enable or disable the collisions on the hand
        /// </summary>
        /// <param name="p_enabled">Whether to enable or disable the collision</param>
        private void CollisionEnable(bool p_enabled)
        {
            if (m_collisionEnabled == p_enabled)
            {
                return;
            }
            m_collisionEnabled = p_enabled;

            if (p_enabled)
            {
                for (int l_i = 0; l_i < m_colliders.Length; ++l_i)
                {
                    m_colliders[l_i].enabled = true;
                }

                for (var l_i = 0; l_i < m_ghostColliders.Length; l_i++)
                {
                    m_ghostColliders[l_i].enabled = true;                    
                }
            }
            else
            {
                for (int l_i = 0; l_i < m_colliders.Length; ++l_i)
                {
                    m_colliders[l_i].enabled = false;
                }

                for (var l_i = 0; l_i < m_ghostColliders.Length; l_i++)
                {
                    m_ghostColliders[l_i].enabled = false;                    
                }
            }
        }

        /// <summary>
        /// Hide the hand when the controller focus is lost
        /// </summary>
        private void OnInputFocusLost()
        {
            if (gameObject.activeInHierarchy)
            {
                m_showAfterInputFocusAcquired.Clear();
                Renderer[] l_renderers = GetComponentsInChildren<Renderer>();
                for (int l_i = 0; l_i < l_renderers.Length; ++l_i)
                {
                    if (l_renderers[l_i].enabled)
                    {
                        l_renderers[l_i].enabled = false;
                        m_showAfterInputFocusAcquired.Add(l_renderers[l_i]);
                    }
                }

                CollisionEnable(false);

                m_restoreOnInputAcquired = true;
            }
        }

        /// <summary>
        /// Show the hand when the controller focus is acquired
        /// </summary>
        private void OnInputFocusAcquired()
        {
            if (m_restoreOnInputAcquired)
            {
                for (int l_i = 0; l_i < m_showAfterInputFocusAcquired.Count; ++l_i)
                {
                    if (m_showAfterInputFocusAcquired[l_i])
                    {
                        m_showAfterInputFocusAcquired[l_i].enabled = true;
                    }
                }
                m_showAfterInputFocusAcquired.Clear();

                CollisionEnable(true);

                m_restoreOnInputAcquired = false;
            }
        }

        /*private void OnTriggerEnter(Collider p_other)
        {
            //We are checking when colliding with another part of any hand, to grab if needed
            if (p_other.isTrigger && (p_other.gameObject.layer == m_leftHandMask || p_other.gameObject.layer == m_rightHandMask))
            {
                TVRLogger.LogMessage("TriggerEnter with "+LayerMask.LayerToName(p_other.gameObject.layer)+" - "+p_other.tag);
                //As we are colliding with another part of the hand, we'll check if we are also colliding with any grabbable
                TVRGrabbable l_grabbable =
                    TVRGrabbableManager.Instance.GetObjectCollidingWith(p_other.bounds);
                if (l_grabbable != null)
                {
                    //If we are, then we are telling it that we are grabbing him
                    l_grabbable.Grab(m_controller);
                }
            }
        }

        private void OnTriggerExit(Collider p_other)
        {
            //We are checking when colliding with another part of any hand, to grab if needed
            if (p_other.isTrigger && (p_other.gameObject.layer == m_leftHandMask || p_other.gameObject.layer == m_rightHandMask))
            {
                //As we are colliding with another part of the hand, we'll check if we are also colliding with any grabbable
                TVRGrabbable l_grabbable =
                    TVRGrabbableManager.Instance.GetObjectCollidingWith(p_other.bounds);
                if (l_grabbable != null)
                {
                    //If we are, then we are telling it that we are dropping him
                    TVRLogger.LogMessage("TriggerExit");
                    l_grabbable.Drop(m_controller);
                }
            }
        }*/
    }
}
