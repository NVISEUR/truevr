﻿using UnityEngine;

namespace HellionCat.TrueVR.Avatar
{
    public class TVRBothHandGrabberController : MonoBehaviour
    {
        [SerializeField] private Transform m_leftHand;
        [SerializeField] private Transform m_rightHand;
        private Transform m_transform;
    
        private Vector3 m_leftHandPos;
        private Vector3 m_rightHandPos;
        private Vector3 m_position;

        private void Start()
        {
            m_transform = transform;
        }

        void FixedUpdate()
        {
            m_position = m_transform.position;
            m_leftHandPos = m_leftHand.position;
            m_rightHandPos = m_rightHand.position;
        
            m_position.x = m_leftHandPos.x + (m_rightHandPos.x - m_leftHandPos.x) / 2;
            m_position.y = m_leftHandPos.y + (m_rightHandPos.y - m_leftHandPos.y) / 2;
            m_position.z = m_leftHandPos.z + (m_rightHandPos.z - m_leftHandPos.z) / 2;

            m_transform.position = m_position;
        }
    }
}
