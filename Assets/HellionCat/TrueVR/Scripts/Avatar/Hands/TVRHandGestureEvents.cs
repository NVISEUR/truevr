﻿using OculusSampleFramework;
using UnityEngine;

public class TVRHandGestureEvents : MonoBehaviour
{
    [SerializeField] Hand m_hand;
    [SerializeField] private float m_pinchedStrengthThreshold = 0.8f;
    [SerializeField] private float m_dropRotationThreshold = 0.05f;

    private Transform m_indexTip;
    private Transform m_middleTip;
    private Transform m_ringTip;
    private Transform m_pinkyTip;

    private bool m_pinchIndex;
    private bool m_pinchMiddle;
    private bool m_pinchRing;
    private bool m_pinchPinky;
    private bool m_tightFist;
    
    public delegate void StartPinchIndex();
    public event StartPinchIndex OnStartPinchedIndex;
    public delegate void StartPinchMiddle();
    public event StartPinchMiddle OnStartPinchedMiddle;
    public delegate void StartPinchRing();
    public event StartPinchRing OnStartPinchedRing;
    public delegate void StartPinchPinky();
    public event StartPinchPinky OnStartPinchedPinky;
    
    public delegate void OnStopGrabbing();
    public event OnStopGrabbing StopGrabbing;
    
    private void Start()
    {
        m_hand.GetComponentInChildren<HandSkeleton>().Initialized += SkeletonOnInitialized;
    }

    private void SkeletonOnInitialized(HandSkeleton p_skeleton)
    {
        m_indexTip = p_skeleton.Bones[(int) HandSkeleton.GetDistalBone(OVRPlugin.BoneId.Hand_IndexTip)];
        m_middleTip = p_skeleton.Bones[(int) HandSkeleton.GetDistalBone(OVRPlugin.BoneId.Hand_MiddleTip)];
        m_ringTip = p_skeleton.Bones[(int) HandSkeleton.GetDistalBone(OVRPlugin.BoneId.Hand_RingTip)];
        m_pinkyTip = p_skeleton.Bones[(int) HandSkeleton.GetDistalBone(OVRPlugin.BoneId.Hand_PinkyTip)];
    }

    void Update()
    {
        //If we are doing a system gesture, do not handle anything else.
        if (m_hand.HandConfidence == Hand.HandTrackingConfidence.Low || !m_indexTip || !m_middleTip || !m_ringTip || !m_pinkyTip)
            return;
        
        if (m_hand.PinchStrength(OVRPlugin.HandFinger.Index) >= m_pinchedStrengthThreshold)
        {
            if (!m_pinchIndex)
            {
                m_pinchIndex = true;
                OnStartPinchedIndex?.Invoke();
                return;
            }
        }
        else
        {
            if (m_pinchIndex)
            {
                m_pinchIndex = false;
                return;
            }
        }
        
        if (m_hand.PinchStrength(OVRPlugin.HandFinger.Middle) >= m_pinchedStrengthThreshold)
        {
            if (!m_pinchMiddle)
            {
                m_pinchMiddle = true;
                OnStartPinchedMiddle?.Invoke();
                return;
            }
        }
        else
        {
            if (m_pinchMiddle)
            {
                m_pinchMiddle = false;
                return;
            }
        }
        
        if (m_hand.PinchStrength(OVRPlugin.HandFinger.Ring) >= m_pinchedStrengthThreshold)
        {
            if (!m_pinchRing)
            {
                m_pinchRing = true;
                OnStartPinchedRing?.Invoke();
                return;
            }
        }
        else
        {
            if (m_pinchRing)
            {
                m_pinchRing = false;
                return;
            }
        }
        
        if(m_hand.PinchStrength(OVRPlugin.HandFinger.Pinky) >= m_pinchedStrengthThreshold)
        {
            if (!m_pinchPinky)
            {
                m_pinchPinky = true;
                OnStartPinchedPinky?.Invoke();
                return;
            }
        }
        else
        {
            if (m_pinchPinky)
            {
                m_pinchPinky = false;
                return;
            }
        }

        if (IsRotationNearZero(m_indexTip.localRotation)
            && IsRotationNearZero(m_middleTip.localRotation)
            && IsRotationNearZero(m_ringTip.localRotation)
            && IsRotationNearZero(m_pinkyTip.localRotation))
        {
            StopGrabbing?.Invoke();
        }
    }

    bool IsRotationNearZero(Quaternion p_value)
    {
        var l_x = p_value.x <= m_dropRotationThreshold && p_value.x > -m_dropRotationThreshold;
        var l_y = p_value.y <= m_dropRotationThreshold && p_value.y > -m_dropRotationThreshold;
        var l_z = p_value.z <= m_dropRotationThreshold && p_value.z > -m_dropRotationThreshold;

        return l_x && l_y && l_z;
    }
}
