﻿using OVRTouchSample;
using UnityEngine;

namespace HellionCat.TrueVR.Avatar
{
    [RequireComponent(typeof(TVRHand))]
    public class TVRHandAnimationController : MonoBehaviour
    {
        private const string AnimLayerNamePoint = "Point Layer";
        private const string AnimLayerNameThumb = "Thumb Layer";
        private const string AnimParamNameFlex = "Flex";
        private const string AnimParamNamePose = "Pose";
        private const float InputRateChange = 20.0f;
        private static readonly int Pinch = Animator.StringToHash("Pinch");
        
        [Header("Settings")]
        [SerializeField] private TVRHand m_hand;
        [SerializeField] private Animator m_animator;
        [SerializeField] private HandPose m_defaultGrabPose;
        
        private int m_animLayerIndexThumb = -1;
        private int m_animLayerIndexPoint = -1;
        private int m_animParamIndexFlex = -1;
        private int m_animParamIndexPose = -1;

        private bool m_isPointing;
        private bool m_isGivingThumbsUp;
        private float m_pointBlend;
        private float m_thumbsUpBlend;

        private void Start()
        {
            // Get animator layer indices by name, for later use switching between hand visuals
            m_animLayerIndexPoint = m_animator.GetLayerIndex(AnimLayerNamePoint);
            m_animLayerIndexThumb = m_animator.GetLayerIndex(AnimLayerNameThumb);
            m_animParamIndexFlex = Animator.StringToHash(AnimParamNameFlex);
            m_animParamIndexPose = Animator.StringToHash(AnimParamNamePose);
        }

        private void Update()
        {
            UpdateCapTouchStates();

            m_pointBlend = InputValueRateChange(m_isPointing, m_pointBlend);
            m_thumbsUpBlend = InputValueRateChange(m_isGivingThumbsUp, m_thumbsUpBlend);

            UpdateAnimStates();
        }

        /// <summary>
        /// Blend the state of the thumbs and index between frames
        /// </summary>
        /// <param name="p_isDown">Whether the button is touched</param>
        /// <param name="p_value">The last frame blend value</param>
        /// <returns>This frame blend value</returns>
        private float InputValueRateChange(bool p_isDown, float p_value)
        {
            float l_rateDelta = Time.deltaTime * InputRateChange;
            float l_sign = p_isDown ? 1.0f : -1.0f;
            return Mathf.Clamp01(p_value + l_rateDelta * l_sign);
        }
        
        /// <summary>
        /// Update the animation states
        /// </summary>
        private void UpdateAnimStates()
        {
            HandPose l_grabPose = m_defaultGrabPose;
            OVRInput.Controller l_controller = m_hand.Controller;
            
            // Pose
            HandPoseId l_handPoseId = l_grabPose.PoseId;
            m_animator.SetInteger(m_animParamIndexPose, (int)l_handPoseId);

            // Flex
            // blend between open hand and fully closed fist
            float l_flex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, l_controller);
            m_animator.SetFloat(m_animParamIndexFlex, l_flex);

            // Point
            bool l_canPoint = l_grabPose.AllowPointing;
            float l_point = l_canPoint ? m_pointBlend : 0.0f;
            m_animator.SetLayerWeight(m_animLayerIndexPoint, l_point);

            // Thumbs up
            bool l_canThumbsUp = l_grabPose.AllowThumbsUp;
            float l_thumbsUp = l_canThumbsUp ? m_thumbsUpBlend : 0.0f;
            m_animator.SetLayerWeight(m_animLayerIndexThumb, l_thumbsUp);

            float l_pinch = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, l_controller);
            m_animator.SetFloat(Pinch, l_pinch);
        }

        /// <summary>
        /// Check the state of the buttons sensor to detect if the player is pointer or giving the thumbs up
        /// </summary>
        private void UpdateCapTouchStates()
        {
            OVRInput.Controller l_controller = m_hand.Controller;
            
            m_isPointing = !OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, l_controller);
            m_isGivingThumbsUp = !OVRInput.Get(OVRInput.NearTouch.PrimaryThumbButtons, l_controller);
        }
    }
}
