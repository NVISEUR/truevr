﻿using System;
using System.Collections;
using System.Collections.Generic;
using HellionCat.TrueVR.Interactable;
using OculusSampleFramework;
using UnityEngine;

public class HandTrackingGrabber : MonoBehaviour
{
    private Hand m_hand;
    public float pinchThreshold = .7f;
    private TVRGrabbable m_grabbedObject;
    
    // Start is called before the first frame update
    protected void Start()
    {
        m_hand = GetComponent<Hand>();
    }

    public void Update()
    {
        if (CheckPinch(OVRPlugin.HandFinger.Index)
            ||CheckPinch(OVRPlugin.HandFinger.Middle)
            ||CheckPinch(OVRPlugin.HandFinger.Ring)
            ||CheckPinch(OVRPlugin.HandFinger.Pinky))
        {
            if (!m_grabbedObject)
            {
                //TryGrabObject();
            }
        }
        else
        {
            if (m_grabbedObject)
            {
                TryDropObject();
            }
        }
    }

    private void HandlePinch()
    {
        if (CheckPinch(OVRPlugin.HandFinger.Index))
        {
            if (!m_grabbedObject)
            {
                //TryGrabObject(m_hand.Skeleton);
            }
        }
        else if (CheckPinch(OVRPlugin.HandFinger.Middle))
        {
            
        }
        else if (CheckPinch(OVRPlugin.HandFinger.Ring))
        {
            
        }
        else if (CheckPinch(OVRPlugin.HandFinger.Pinky))
        {
            
        }
        else
        {
            if (m_grabbedObject)
            {
                TryDropObject();
            }
        }
    }

    private bool CheckPinch(OVRPlugin.HandFinger p_finger)
    {
        float pinchStrength = m_hand.PinchStrength(p_finger);
        return pinchStrength > pinchThreshold;
    }
    
    private void TryGrabObject(Vector3 p_centerPoint)
    {
        UnityEngine.Debug.Log("Pinch");
        m_grabbedObject =
            TVRGrabbableManager.Instance.GetObjectIntersecting(new Bounds());
        if (m_grabbedObject != null)
        {
            UnityEngine.Debug.Log("Try grab");
            //If we are, then we are telling it that we are grabbing him
            m_grabbedObject.Grab(m_hand.HandType);
        }
    }

    private void TryDropObject()
    {
        UnityEngine.Debug.Log("Unpinch");
        if (m_grabbedObject != null)
        {
            UnityEngine.Debug.Log("Try drop");
            m_grabbedObject.Drop(m_hand.HandType);
            m_grabbedObject = null;
        }
    }
}
