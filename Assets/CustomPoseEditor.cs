﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[CustomEditor(typeof(TVRCustomPose))]
public class CustomPoseEditor: UnityEditor.Editor 
{
    private SerializedProperty CustomLocalPosition;
    private SerializedProperty Data;

    private void OnEnable()
    {
        // Link the properties
        CustomLocalPosition = serializedObject.FindProperty("m_customLocalPosition");
        Data = serializedObject.FindProperty("m_data");
    }

    public override void OnInspectorGUI() 
    {
        // Load the real class values into the serialized copy
        serializedObject.Update();

        // Automatically uses the according PropertyDrawer for the type
        EditorGUILayout.PropertyField(CustomLocalPosition);

        EditorGUIUtility.labelWidth = 0.001f;
        for (int i = 0; i < Data.arraySize; i++)
        {
            SerializedProperty boneId = Data.GetArrayElementAtIndex(i).FindPropertyRelative("BoneId");
            SerializedProperty rotation = Data.GetArrayElementAtIndex(i).FindPropertyRelative("Rotation");
            SerializedProperty frozen = Data.GetArrayElementAtIndex(i).FindPropertyRelative("Frozen");
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.LabelField(new GUIContent(boneId.enumDisplayNames[boneId.enumValueIndex]),EditorStyles.boldLabel );

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Is frozen", GUILayout.MaxWidth(170));
            EditorGUILayout.LabelField("Local rotation", GUILayout.MaxWidth(100));
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.PropertyField(boneId,new GUIContent(""));
            EditorGUILayout.PropertyField(frozen,new GUIContent(""), GUILayout.MaxWidth(75));
            EditorGUILayout.PropertyField(rotation,new GUIContent(""), GUILayout.MaxWidth(250));
            EditorGUILayout.EndHorizontal();
        }

        // Write back changed values and evtl mark as dirty and handle undo/redo
        serializedObject.ApplyModifiedProperties();
    }
}
#endif