﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CustomPose", menuName = "TVR/CustomPose", order = 1)]
public class TVRCustomPose : ScriptableObject
{
    [SerializeField] private Vector3 m_customLocalPosition;
    [SerializeField] private BoneData[] m_data;

    public Vector3 CustomLocalPosition => m_customLocalPosition;

    public BoneData[] Data => m_data;

    private void Reset()
    {
        m_data = new BoneData[(int)OVRPlugin.BoneId.Hand_MaxSkinnable];
        for (var l_i = 1; l_i <= m_data.Length; l_i++)
        {
            m_data[l_i-1] = new BoneData
            {
                BoneId = (OVRPlugin.BoneId)Enum.GetValues(typeof(OVRPlugin.BoneId)).GetValue(l_i),
                Rotation = Vector3.zero,
                Frozen = true
            };
        }
    }
}

[Serializable]
public struct BoneData
{
    public OVRPlugin.BoneId BoneId;
    public Vector3 Rotation;
    public bool Frozen;
}
