﻿using System.Collections;
using System.Collections.Generic;
using HellionCat.TrueVR.Debug;
using UnityEngine;

public class TestPinch : MonoBehaviour
{
    [SerializeField] OVRHand m_hand;

    // Update is called once per frame
    void Update()
    {
        if (m_hand.GetFingerIsPinching(OVRHand.HandFinger.Index))
        {
            Debug.Log("Pinching Index");
        }
        if (m_hand.GetFingerIsPinching(OVRHand.HandFinger.Middle))
        {
            Debug.Log("Pinching Middle");
        }
        if (m_hand.GetFingerIsPinching(OVRHand.HandFinger.Thumb))
        {
            Debug.Log("Pinching Thumb");
        }
        if (m_hand.GetFingerIsPinching(OVRHand.HandFinger.Pinky))
        {
            Debug.Log("Pinching Pinky");
        }
        if (m_hand.GetFingerIsPinching(OVRHand.HandFinger.Ring))
        {
            Debug.Log("Pinching Ring");
        }
    }
}
