﻿using System;
using System.Collections;
using System.Collections.Generic;
using OculusSampleFramework;
using UnityEngine;

public class BoneTriggerHandler : MonoBehaviour
{
    private HandSkeleton m_skeleton;
    private short m_boneIndex;

    public void InitializeBone(HandSkeleton p_skeleton,short p_boneIndex)
    {
        m_skeleton = p_skeleton;
        m_boneIndex = p_boneIndex;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LeftHandColl") || other.CompareTag("RightHandColl")
            || other.name == "HandLeft" || other.name == "HandRight")
            return;
        
        m_skeleton.ChangeBoneFreezingState(m_boneIndex,true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("LeftHandColl") || other.CompareTag("RightHandColl")
            || other.name == "HandLeft" || other.name == "HandRight")
            return;
        
        m_skeleton.ChangeBoneFreezingState(m_boneIndex,false);
    }
}
