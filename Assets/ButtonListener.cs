﻿using System.Collections;
using System.Collections.Generic;
using OculusSampleFramework;
using UnityEngine;
using UnityEngine.Events;

public class ButtonListener : MonoBehaviour
{
    public UnityEvent m_proximityEvent;
    public UnityEvent m_contactEvent;
    public UnityEvent m_actionEvent;
    public UnityEvent m_defaultEvent;
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ButtonController>().InteractableStateChanged.AddListener(InitiateEvent);
    }

    private void InitiateEvent(InteractableStateArgs p_state)
    {
        if (p_state.NewInteractableState == InteractableState.ProximityState)
        {
            m_proximityEvent.Invoke();
        }
        else if (p_state.NewInteractableState == InteractableState.ContactState)
        {
            m_contactEvent.Invoke();
        }
        else if (p_state.NewInteractableState == InteractableState.ActionState)
        {
            m_actionEvent.Invoke();
        }
        else
        {
            m_defaultEvent.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
